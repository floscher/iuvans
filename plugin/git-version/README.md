> [iuvans](../../README.md) › [plugin](../README.md) › **git-version**

# git-version

This plugin adds an extension `gitVersion` to your Gradle build,
which allows you to easily construct a version number from the git repository that
your Gradle build resides in.

It starts at the project directory and searches for a git repository.
If none is found it walks the parent directories up to the file system root and searches there as well.
The first git repository that it finds is used to determine the version.

## Apply the plugin

```kotlin
plugins {
  id("dev.spinf.iuvans.git-version").version("$version")
}
```

Usage:
```kotlin
project.gitVersion.versionName.get() // a version string like what `git describe --always --dirty` returns
project.gitVersion.versionCode.get() // the number of ancestor commits of the current commit (plus one for the current commit)
project.gitVersion.commitHash.get() // the full commit hash of the current commit
project.gitVersion.abbrevCommitHash.get() // the uniquely abbreviated commit hash of the current commit (by default 7 chars, if unique within the repo)
```

These fields will throw an `java.io.IOException` if the Gradle project is not inside a git repository.
