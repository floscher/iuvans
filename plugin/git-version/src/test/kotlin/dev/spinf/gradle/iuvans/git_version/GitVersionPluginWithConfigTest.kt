package dev.spinf.gradle.iuvans.git_version

import dev.spinf.gradle.iuvans.testhelper.GradleRunnerUtil
import dev.spinf.gradle.iuvans.testhelper.commit
import dev.spinf.gradle.iuvans.testhelper.initGitInDir
import dev.spinf.gradle.iuvans.testhelper.stageFile
import dev.spinf.gradle.iuvans.testhelper.tag
import dev.spinf.gradle.iuvans.testhelper.writeTextFile
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo

class GitVersionPluginWithConfigTest {
  val buildGradleKts = GitVersionPluginWithConfigTest::class.java.getResource("build.gradle.kts")!!.readText()

  @Test
  fun testConfigWithoutGitRepo(testInfo: TestInfo) {
    val runner = GradleRunnerUtil.createGradleRunnerInTmpDir(testInfo)

    runner.projectDir.writeTextFile("build.gradle.kts", buildGradleKts)

    println(
      runner.withEnvironment(
        OptionsForTest("something", true, true, true).asEnvVars
      ).build().output
    )
  }

  @Test
  fun testWithConfig(testInfo: TestInfo) {
    val runner = GradleRunnerUtil.createGradleRunnerInTmpDir(testInfo)
    val git = runner.projectDir.initGitInDir()
    runner.projectDir.writeTextFile("build.gradle.kts", buildGradleKts)
    runner.projectDir.writeTextFile(".gitignore", """
      .gitignore
      *.gradle.kts
    """.trimIndent())

    runner.withEnvironment(
      OptionsForTest("something", true, true, true).asEnvVars +
      ExpectedValues(hasUncommittedChanges = false).asEnvVars
    ).build()

    runner.projectDir.writeTextFile("text.txt", "text\n")
    git.stageFile("text.txt").commit("Initial commit").tag("v1.2.3")

    println(
      runner.withEnvironment(
        OptionsForTest("something", true, true, true).asEnvVars +
        ExpectedValues("1.2.3something", 1u, false, "155462bde68801845056f37d80749b53c755385a").asEnvVars
      ).build().output
    )
  }
}
