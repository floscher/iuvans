package dev.spinf.gradle.iuvans.git_version

import dev.spinf.gradle.iuvans.testhelper.GradleRunnerUtil
import dev.spinf.gradle.iuvans.testhelper.appendToTextFile
import dev.spinf.gradle.iuvans.testhelper.commit
import dev.spinf.gradle.iuvans.testhelper.initGitInDir
import dev.spinf.gradle.iuvans.testhelper.stageFile
import dev.spinf.gradle.iuvans.testhelper.tag
import dev.spinf.gradle.iuvans.testhelper.writeTextFile
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo

class GitVersionPluginTest {
  private val buildGradleKtsContent = GitVersionPluginTest::class.java.getResource("build.gradle.kts")!!.readText()

  @Test
  fun allNullWithoutGitRepo(testinfo: TestInfo) {
    val gradleRunner = GradleRunnerUtil.createGradleRunnerInTmpDir(testinfo)

    gradleRunner.projectDir.writeTextFile("build.gradle.kts", buildGradleKtsContent)

    gradleRunner.withEnvironment(ExpectedValues().asEnvVars).build()
  }

  @Test
  fun ensureBasicGitVersionValues(testInfo: TestInfo) {
    val runner = GradleRunnerUtil.createGradleRunnerInTmpDir(testInfo)
    val git = runner.projectDir.initGitInDir()
    val projectDir = runner.projectDir

    // add the untracked files build.gradle.kts, .gitignore and untracked.txt
    projectDir.writeTextFile(".gitignore", """
      .gitignore
      *.kts
    """.trimIndent())
    projectDir.writeTextFile("build.gradle.kts", buildGradleKtsContent)
    projectDir.writeTextFile("untracked.txt", "this file is untracked\n")
    // Create the initial commit containing only the file text.txt
    git.stageFile("text.txt", "text\n").commit("Initial commit")

    runner.withEnvironment(ExpectedValues(
      "155462b",
      1u,
      false,
      "155462bde68801845056f37d80749b53c755385a",
    ).asEnvVars).build()

    // Append to text.txt without committing
    projectDir.appendToTextFile("text.txt", "more text\n")

    runner.withEnvironment(ExpectedValues(
      "155462b-SNAPSHOT",
      1u,
      true,
      "155462bde68801845056f37d80749b53c755385a",
    ).asEnvVars).build()

    // Create an unannotated tag
    git.tag("v1.0.0")

    runner.withEnvironment(ExpectedValues(
      "155462b-SNAPSHOT",
      1u,
      true,
      "155462bde68801845056f37d80749b53c755385a",
    ).asEnvVars).build()

    // Create an annotated tag
    git.tag("v1.2.3", "This is an annotated tag")
    git.add().addFilepattern("text.txt").call()

    runner.withEnvironment(ExpectedValues(
      "1.2.3-SNAPSHOT",
      1u,
      true,
      "155462bde68801845056f37d80749b53c755385a",
    ).asEnvVars).build()

    git.commit("Second commit")

    runner.withEnvironment(ExpectedValues(
      "1.2.3-1-g9fdfb6f",
      2u,
      false,
      "9fdfb6f9a1cb7841a9cb0c44fdade8b62a59fbcd",
    ).asEnvVars).build()

    projectDir.appendToTextFile("text.txt", "even more text\n")

    runner.withEnvironment(ExpectedValues(
      "1.2.3-1-g9fdfb6f-SNAPSHOT",
      2u,
      true,
      "9fdfb6f9a1cb7841a9cb0c44fdade8b62a59fbcd",
    ).asEnvVars).build()

    git.add().addFilepattern("text.txt").call()
    git.commit("Third commit")

    runner.withEnvironment(ExpectedValues(
      "1.2.3-2-g29829d3",
      3u,
      false,
      "29829d3b89859ea9da6288fcc26eefa35a4841a7"
    ).asEnvVars).build()

    git.tag("1.2.4", "This is another annotated tag")

    runner.withEnvironment(ExpectedValues(
      "1.2.4",
      3u,
      false,
      "29829d3b89859ea9da6288fcc26eefa35a4841a7"
    ).asEnvVars).build()
  }
}
