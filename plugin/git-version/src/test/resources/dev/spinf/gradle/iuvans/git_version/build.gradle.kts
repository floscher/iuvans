plugins {
  id("dev.spinf.iuvans.git-version")
}

gitVersion {
  config {
    System.getenv("CONFIG_DIRTY_SUFFIX")?.let { dirtySuffix.set(it) }
    System.getenv("CONFIG_IS_CONSIDERING_ALL_TAGS")?.let { isConsideringAllTags.set(it.toBooleanStrict()) }
    System.getenv("CONFIG_IS_TREATING_UNTRACKED_FILES_AS_DIRTY")?.let { isTreatingUntrackedFilesAsDirty.set(it.toBooleanStrict()) }
    System.getenv("CONFIG_IS_TRIMMING_V_PREFIX")?.let { isTrimmingVPrefix.set(it.toBooleanStrict()) }
  }

  assertEquals("commitHash", System.getenv("COMMIT_HASH"), commitHash.orNull)
  assertEquals("abbreviatedCommitHash", System.getenv("ABBREVIATED_COMMIT_HASH"), abbreviatedCommitHash.orNull)
  assertEquals("versionName", System.getenv("VERSION_NAME"), versionName.orNull)
  assertEquals("versionCode", System.getenv("VERSION_CODE")?.toInt(), versionCode.orNull)
  assertEquals("hasUncommittedChanges", System.getenv("HAS_UNCOMMITTED_CHANGES")?.toBooleanStrict(), hasUncommittedChanges.orNull)
}

fun <T> assertEquals(label: String, expected: T, actual: T) {
  require(expected == actual) {
    "Expected $label ${expected?.let { "'$expected'" }}, but was ${actual?.let { "'$actual'" }}!"
  }
  logger.lifecycle("$label has actually the expected value of ${expected?.let { "'$it'" }}")
}
