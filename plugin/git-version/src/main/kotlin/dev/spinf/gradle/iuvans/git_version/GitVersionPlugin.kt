package dev.spinf.gradle.iuvans.git_version

import org.gradle.api.Plugin
import org.gradle.api.Project

class GitVersionPlugin: Plugin<Project> {
  override fun apply(target: Project) {
    target.extensions.create("gitVersion", GitVersionExtension::class.java)
  }
}
