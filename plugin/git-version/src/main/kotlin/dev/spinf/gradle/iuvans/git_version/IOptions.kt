package dev.spinf.gradle.iuvans.git_version

interface IOptions {
  val dirtySuffix: String?
  val isConsideringAllTags: Boolean?
  val isTreatingUntrackedFilesAsDirty: Boolean?
  val isTrimmingVPrefix: Boolean?
}
