package dev.spinf.gradle.iuvans.git_version

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.errors.NoHeadException
import org.eclipse.jgit.api.errors.RefNotFoundException
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import java.io.IOException
import javax.inject.Inject

open class GitVersionExtension @Inject constructor(project: Project) {

  val config: GitVersionConfig = GitVersionConfig(project)
  fun config(configAction: Action<GitVersionConfig>) = configAction.execute(this.config)

  /**
   * @throws IOException when no git directory is found
   */
  private val git: Git? by lazy {
    try {
      val fileRepo = FileRepositoryBuilder().readEnvironment().findGitDir(project.projectDir).takeIf { it.gitDir != null }
      if (fileRepo == null) {
        project.logger.warn("No git version repository found in ${project.projectDir.absolutePath}")
        null
      } else {
        Git(fileRepo.build())
      }
    } catch (e: IOException) {
      project.logger.warn("Could not determine Git directory!", e)
      null
    }
  }

  private val headCommit: Provider<ObjectId> = project.provider {
    git?.repository?.resolve(Constants.HEAD)
  }

  /**
   * The full SHA1 commit hash (42 chars) of the current commit.
   */
  val commitHash: Provider<String> = headCommit.map { it.name }

  /**
   * A boolean that is true iff there are changes made in the repository since the last commit.
   * Changes to files that are ignored via a `.gitignore` file are not considered.
   */
  val hasUncommittedChanges: Provider<Boolean> = project.provider {
    git?.status()?.call()?.hasUncommittedChanges()
  }

  /**
   * A unique abbreviation of [commitHash], by default the first seven characters, or more if needed to make it unique.
   */
  val abbreviatedCommitHash: Provider<String> = headCommit.flatMap { headCommit ->
    project.provider {
      git?.repository?.newObjectReader()?.abbreviate(headCommit)?.name()
    }
  }

  /**
   * A numeric version code, which is the number of all ancestral commits of the current commit
   * (plus 1 for the current commit).
   */
  val versionCode: Provider<Int> = project.provider {
    try {
      git?.log()?.call()?.count()
    } catch (e: NoHeadException) {
      project.logger.warn("Git repository without HEAD (probably no commits yet).", e)
      null
    }
  }

  /**
   * A version name for the current commit.
   * If there are no tags on the current commit or its ancestors, it is just the abbreviated commit hash.
   *
   */
  val versionName: Provider<String> = project.provider {
    val git = this.git
    if (git == null) {
      null
    } else {
      val options = config.options.get()
      try {
        git.describe()
          .setAlways(true)
          .setTags(options.isConsideringAllTags)
          .call()
          .let { // trimming v-prefix
            if (options.isTrimmingVPrefix && it.length > 1) {
              it.removePrefix("v").removePrefix("V")
            } else it
          }
          .let { // adding dirty suffix
            val status = git.status().call()
            if (status.hasUncommittedChanges() || (options.isTreatingUntrackedFilesAsDirty && status.untracked.isNotEmpty())) {
              it + options.dirtySuffix
            } else it
          }
      } catch (e: RefNotFoundException) {
        project.logger.warn("Git repository without HEAD commit (probably without any commits yet).", e)
        null
      }
    }
  }

  fun isGitAvailable() = git !== null
}
