package dev.spinf.gradle.iuvans.git_version

internal data class Options(
  override val dirtySuffix: String,
  override val isConsideringAllTags: Boolean,
  override val isTreatingUntrackedFilesAsDirty: Boolean,
  override val isTrimmingVPrefix: Boolean
): IOptions
