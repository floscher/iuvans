package dev.spinf.gradle.iuvans.git_version

import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider

class GitVersionConfig(project: Project) {
  companion object {
    const val DIRTY_SUFFIX_GIT = "-dirty"
    const val DIRTY_SUFFIX_MAVEN = "-SNAPSHOT"
  }

  /**
   * The suffix that is appended to the version number, when there are uncommitted changes.
   * See also [useGitStyleDirtySuffix] and [useMavenStyleDirtySuffix] for setting predefined suffixes.
   *
   * Default value is [DIRTY_SUFFIX_MAVEN], the Maven convention of `-SNAPSHOT`.
   */
  val dirtySuffix: Property<String> = project.createProperty(convention = DIRTY_SUFFIX_MAVEN)

  /**
   * This corresponds with the `--tags` option of `git describe`.
   * If set to `true`, then unannotated tags are also used to describe a version.
   *
   * Default value is `false`, so only annotated tags are considered.
   */
  val isConsideringAllTags: Property<Boolean> = project.createProperty(convention = false)

  /**
   * When `true`, then the [dirtySuffix] is also appended, when there are files that are neither committed nor ignored.
   *
   * Default value is `false`.
   */
  val isTreatingUntrackedFilesAsDirty: Property<Boolean> = project.createProperty(convention = false)

  /**
   * Flag indicating if one leading `v` (or `V`) character is removed from the beginning of the version number.
   * If there is no leading `v` character, this has no effect.
   *
   * Default value is `true`.
   */
  val isTrimmingVPrefix: Property<Boolean> = project.createProperty(convention = true)

  /**
   * @return a provider with all the current settings, always returns a non-null value
   */
  internal val options: Provider<Options> = dirtySuffix.flatMap { suffix ->
    isConsideringAllTags.flatMap { allTags ->
      isTreatingUntrackedFilesAsDirty.flatMap { untracked ->
        isTrimmingVPrefix.map { trimV ->
          Options(suffix, allTags, untracked, trimV)
        }
      }
    }
  }

  /**
   * Convenience method for setting [dirtySuffix] to [DIRTY_SUFFIX_GIT].
   */
  fun useGitStyleDirtySuffix() = dirtySuffix.set(DIRTY_SUFFIX_GIT)

  /**
   * Convenience method for setting [dirtySuffix] to [DIRTY_SUFFIX_MAVEN].
   */
  fun useMavenStyleDirtySuffix() = dirtySuffix.set(DIRTY_SUFFIX_MAVEN)

  private inline fun <reified T> Project.createProperty(convention: T) =
    objects.property(T::class.java).convention(convention).apply { finalizeValueOnRead() }
}
