package dev.spinf.gradle.iuvans.git_version

data class OptionsForTest(
  override val dirtySuffix: String? = null,
  override val isConsideringAllTags: Boolean? = null,
  override val isTreatingUntrackedFilesAsDirty: Boolean? = null,
  override val isTrimmingVPrefix: Boolean? = null
): IOptions, IEnvVars {
  override val asEnvVars: Map<String, String> = listOfNotNull(
    dirtySuffix?.let { "CONFIG_DIRTY_SUFFIX" to it },
    isConsideringAllTags?.let { "CONFIG_IS_CONSIDERING_ALL_TAGS" to it.toString() },
    isTreatingUntrackedFilesAsDirty?.let { "CONFIG_IS_TREATING_UNTRACKED_FILES_AS_DIRTY" to it.toString() },
    isTrimmingVPrefix?.let { "CONFIG_IS_TRIMMING_V_PREFIX" to it.toString() },
  ).toMap()
}
