package dev.spinf.gradle.iuvans.git_version

import kotlin.math.min

data class ExpectedValues(
  val versionName: String? = null,
  val versionCode: UInt? = null,
  val hasUncommittedChanges: Boolean? = null,
  val commitHash: String? = null,
  val abbreviatedCommitHash: String? = commitHash?.let { it.substring(0 until min(it.length, 7)) },
): IEnvVars {
  override val asEnvVars: Map<String, String> = listOfNotNull(
    versionName?.let { "VERSION_NAME" to it },
    versionCode?.let { "VERSION_CODE" to it.toString() },
    hasUncommittedChanges?.let { "HAS_UNCOMMITTED_CHANGES" to it.toString() },
    commitHash?.let { "COMMIT_HASH" to it },
    abbreviatedCommitHash?.let { "ABBREVIATED_COMMIT_HASH" to it },
  ).toMap()
}
