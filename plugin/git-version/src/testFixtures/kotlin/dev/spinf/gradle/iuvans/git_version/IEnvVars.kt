package dev.spinf.gradle.iuvans.git_version

interface IEnvVars {
  val asEnvVars: Map<String, String>
}
