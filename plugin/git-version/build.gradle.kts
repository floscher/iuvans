plugins {
  `java-test-fixtures`
}

dependencies {
  implementation(libs.jgit)

  testImplementation(project(":testhelper"))
  testImplementation(gradleTestKit())
  testImplementation(libs.junit.api)
  testImplementation(project(":plugin:git-version"))
  testRuntimeOnly(libs.junit.engine)
}

gradlePlugin.plugins.named(project.gradlePluginName) {
  description = "Makes the value of `git describe` available to the Gradle build, as well as the number of commits so far."
}
