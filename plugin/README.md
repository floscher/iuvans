> [iuvans](../README.md) › **plugin**

## Individual plugins

* [git-version](git-version/README.md) – adds easy access to git version information
* [markdown](markdown/README.md) – adds a task to convert markdown to HTML

The plugins can be applied like this:
```kotlin
plugins {
  id("dev.spinf.iuvans.$name").version("$version")
}
```
(where `$name` is the plugin name and `$version` is the current version number of iuvans)

## Bundle of all plugins

* [iuvans](plugin/iuvans/README.md) – all the plugins above bundled into one Gradle plugin
