project.subprojects {
  apply(plugin = "com.gradle.plugin-publish")
  apply(plugin = "java-gradle-plugin")
  apply(plugin = "maven-publish")
  apply(plugin = "org.jetbrains.kotlin.jvm")

  repositories.mavenCentral()

  registerGradlePlugin()

  extensions.getByType(PublishingExtension::class).apply {
    repositories {
      maven(rootProject.layout.buildDirectory.map { it.file("maven").asFile }.get().toURI()) {
        name = "buildDir"
      }
    }
  }
}
