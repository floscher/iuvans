package dev.spinf.gradle.iuvans.markdown

import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.tasks.TaskProvider

interface MarkdownSourceDirectorySet : SourceDirectorySet {
  val compileTask: TaskProvider<MarkdownCompile>
}
