package dev.spinf.gradle.iuvans.markdown

import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskContainer

internal fun TaskContainer.registerMarkdownCompileTask(name: String, sourceSet: SourceSet, sourceDirectorySet: SourceDirectorySet) = register(
  name,
  MarkdownCompile::class.java,
  sourceSet,
  sourceDirectorySet
)
