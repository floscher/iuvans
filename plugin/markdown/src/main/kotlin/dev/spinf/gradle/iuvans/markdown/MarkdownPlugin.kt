package dev.spinf.gradle.iuvans.markdown

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JvmEcosystemPlugin
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer

class MarkdownPlugin: Plugin<Project> {
  override fun apply(target: Project) {
    target.plugins.withType(JvmEcosystemPlugin::class.java).whenPluginAdded {
      target.extensions.getByType(SourceSetContainer::class.java).withType(SourceSet::class.java).all { sourceSet ->
        sourceSet.extensions.create("markdown", CustomMarkdownSourceSetExtension::class.java, target, sourceSet)
      }
    }
  }
}
