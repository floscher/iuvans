package dev.spinf.gradle.iuvans.markdown;

import org.gradle.api.file.Directory
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceTask
import org.gradle.api.tasks.TaskAction
import org.intellij.markdown.IElementType
import org.intellij.markdown.MarkdownElementTypes
import org.intellij.markdown.ast.ASTNode
import org.intellij.markdown.ast.getTextInNode
import org.intellij.markdown.flavours.MarkdownFlavourDescriptor
import org.intellij.markdown.flavours.gfm.GFMFlavourDescriptor
import org.intellij.markdown.html.GeneratingProvider
import org.intellij.markdown.html.HtmlGenerator
import org.intellij.markdown.html.InlineLinkGeneratingProvider
import org.intellij.markdown.html.LinkGeneratingProvider
import org.intellij.markdown.html.ReferenceLinksGeneratingProvider
import org.intellij.markdown.html.URI
import org.intellij.markdown.parser.LinkMap
import org.intellij.markdown.parser.MarkdownParser
import java.io.File
import java.util.Locale
import javax.inject.Inject


open class MarkdownCompile @Inject constructor(private val sourceSet: SourceSet, private val sourceDirectorySet: SourceDirectorySet): SourceTask() {

  @OutputDirectory
  val outputDirectory: Provider<Directory> = project.layout.buildDirectory.dir("markdown/${sourceSet.name}/${sourceDirectorySet.name}")

  init {
    group = "Markdown"
    source(sourceDirectorySet)
  }

  final override fun source(vararg sources: Any?): SourceTask = super.source(sources)

  @TaskAction
  public fun action() {
    val outputDirectory = this.outputDirectory.get()
    val buildDir = project.layout.buildDirectory.get()
    require(outputDirectory.asFile.hasMatchingParentFile { buildDir.asFile == it }) {
      "The output directory ${outputDirectory.asFile} must be inside the build directory ${buildDir.asFile}!"
    }
    project.delete(outputDirectory)
    logger.lifecycle("Compile markdown for ${sourceDirectorySet}")

    logger.lifecycle("Compiling Markdown …")
    source.visit {
      if (!it.isDirectory) {
        val markdownText = it.file.readText()
        val targetFile = it.relativePath.getFile(outputDirectory.asFile)
          .let { f -> f.parentFile.resolve("${f.nameWithoutExtension}.html") }
        val flavour = CustomFlavour(GFMFlavourDescriptor())
        val parsedTree = MarkdownParser(flavour).buildMarkdownTreeFromString(markdownText)
        val html = HtmlGenerator(markdownText, parsedTree, flavour).generateHtml()
        require(targetFile.parentFile.mkdirs() || targetFile.parentFile.canRead()) {
          "Could not create directory ${targetFile.parent}"
        }
        targetFile.writeText(html)
        logger.lifecycle("  \uD83D\uDDB9 ${it.relativePath} → \uD83D\uDDBA ${targetFile.absolutePath}")
      }
    }
    logger.lifecycle("Done compiling.")
  }

  private fun File.hasMatchingParentFile(predicate: (File) -> Boolean): Boolean = if (parentFile == null) {
    false
  } else if (predicate(parentFile)) {
    true
  } else {
    parentFile.hasMatchingParentFile(predicate)
  }

  companion object {
    fun createTaskName(sourceSet: SourceSet, sourceDirectorySet: SourceDirectorySet) =
      "compile${sourceSet.name.capitalizeOrEmptyWhen(SourceSet.MAIN_SOURCE_SET_NAME)}Markdown${sourceDirectorySet.name.capitalizeOrEmptyWhen("markdown")}"


    private fun String.capitalizeOrEmptyWhen(emptyCase: String) = capitalize().takeIf { it != emptyCase.capitalize() } ?: ""
    private fun String.capitalize() = replaceFirstChar { if (it.isTitleCase()) it.toString() else it.titlecase(Locale.ROOT)  }
  }
  private class CustomFlavour(private val delegate: GFMFlavourDescriptor) : MarkdownFlavourDescriptor by delegate {
    override fun createHtmlGeneratingProviders(linkMap: LinkMap, baseURI: URI?): Map<IElementType, GeneratingProvider> {
      return delegate.createHtmlGeneratingProviders(linkMap, baseURI) + mapOf(
        MarkdownElementTypes.INLINE_LINK to LP(null),
        MarkdownElementTypes.FULL_REFERENCE_LINK to RP(linkMap, null),
        //GFMTokenTypes.GFM_AUTOLINK to UP()
      )
    }
  }

  private class UP(): LinkGeneratingProvider(null, true) {
    override fun getRenderInfo(text: String, node: ASTNode): RenderInfo? {
      println("UP " + node.getTextInNode(text) + " # " + node.children.map { it.type.toString() }.joinToString())
      return RenderInfo(node, "dest", "title")
    }

  }

  private class LP(baseURI: URI?): InlineLinkGeneratingProvider(baseURI, true) {
    override fun getRenderInfo(text: String, node: ASTNode): RenderInfo? {
      println(node.getTextInNode(text))
      val ri = super.getRenderInfo(text, node)
      return ri?.let { RenderInfo(it.label, it.destination.let { if (it.endsWith(".md")) it.removeSuffix(".md").toString().plus(".html") else it }, it.title)}
    }
  }

  private class RP(linkMap: LinkMap, baseURI: URI?): ReferenceLinksGeneratingProvider(linkMap, baseURI, true) {
    override fun getRenderInfo(text: String, node: ASTNode): RenderInfo? {
      return super.getRenderInfo(text, node)?.let {
        RenderInfo(
          it.label,
          it.destination.let { if (it.endsWith(".md")) it.removeSuffix(".md").toString().plus(".html") else it },
          it.title
        )
      }
    }
  }
}
