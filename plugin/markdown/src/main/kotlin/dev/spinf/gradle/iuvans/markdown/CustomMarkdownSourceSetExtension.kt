package dev.spinf.gradle.iuvans.markdown

import org.gradle.api.Project
import org.gradle.api.internal.tasks.DefaultTaskDependencyFactory
import org.gradle.api.tasks.SourceSet
import javax.inject.Inject

open class CustomMarkdownSourceSetExtension @Inject constructor(
  private val project: Project,
  val sourceSet: SourceSet
) :
    (String, (MarkdownSourceDirectorySet).() -> Unit) -> Unit,
    (String) -> Unit,
    () -> Unit {

  @Inject
  var taskDependencyFactory: DefaultTaskDependencyFactory? = null

  override fun invoke(name: String, config: (MarkdownSourceDirectorySet).() -> Unit) {
    val sourceDirectorySet = DefaultMarkdownSourceDirectorySet(
      project,
      sourceSet,
      name,
      requireNotNull(taskDependencyFactory) { "Error injecting task dependency factory!" }
    )
    sourceSet.allSource.source(sourceDirectorySet)
    project.tasks.named(sourceSet.classesTaskName) {
      it.dependsOn(sourceDirectorySet.compileTask)
    }
  }

  override fun invoke(name: String) = invoke(name) { }

  override fun invoke() = invoke("markdown") { }
}
