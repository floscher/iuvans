package dev.spinf.gradle.iuvans.markdown

import org.gradle.api.Project
import org.gradle.api.internal.file.DefaultSourceDirectorySet
import org.gradle.api.internal.tasks.DefaultTaskDependencyFactory
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskProvider
import javax.inject.Inject

open class DefaultMarkdownSourceDirectorySet @Inject constructor(
  project: Project,
  sourceSet: SourceSet,
  name: String,
  taskDependencyFactory: DefaultTaskDependencyFactory
): MarkdownSourceDirectorySet, DefaultSourceDirectorySet(project.objects.sourceDirectorySet(name, "Markdown src/${sourceSet.name}/$name"), taskDependencyFactory) {

  override val compileTask: TaskProvider<MarkdownCompile> = project.tasks.registerMarkdownCompileTask(
    MarkdownCompile.createTaskName(
      sourceSet,
      this
    ),
    sourceSet,
    this
  )

  init {
    setSrcDirs(setOf(project.layout.projectDirectory.dir("src/${sourceSet.name}/$name")))
    filter.include("**/*.md")
  }

  final override fun setSrcDirs(srcPaths: Iterable<*>) = super.setSrcDirs(srcPaths)
}
