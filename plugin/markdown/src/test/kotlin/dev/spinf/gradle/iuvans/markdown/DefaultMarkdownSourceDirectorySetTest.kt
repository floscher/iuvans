package dev.spinf.gradle.iuvans.markdown

import io.mockk.every
import io.mockk.mockk
import org.gradle.api.Project
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.internal.file.DefaultSourceDirectorySet
import org.gradle.api.model.ObjectFactory
import org.gradle.api.tasks.SourceSet
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class DefaultMarkdownSourceDirectorySetTest {

  val sourceDirectorySet: DefaultSourceDirectorySet = mockk()
  val objects: ObjectFactory = mockk()
  val project: Project = mockk()
  val sourceSet: SourceSet = mockk()

  init {
    every { project.objects }.returns(objects)
    every { objects.sourceDirectorySet(any(), any()) }.returns(sourceDirectorySet)
    every { sourceSet.name }.returns("main")
  }

  @Test
  fun test() {
    assertTaskNamed("compileMarkdown", "main", "markdown")
    assertTaskNamed("compileMarkdown", "Main", "Markdown")
    assertTaskNamed("compileAMarkdownB", "a", "b")
  }

  fun assertTaskNamed(expectedTaskName: String, sourceSetName: String, sourceDirectorySetName: String) {
    val sourceSet: SourceSet = mockk()
    every { sourceSet.name }.returns(sourceSetName)
    val sourceDirectorySet: SourceDirectorySet = mockk()
    every { sourceDirectorySet.name }.returns(sourceDirectorySetName)
    assertEquals(expectedTaskName, MarkdownCompile.createTaskName(sourceSet, sourceDirectorySet))
  }
}
