plugins {
  kotlin("jvm")
}

repositories {
  mavenCentral()
}
dependencies {
  implementation(libs.jetbrains.markdown)
  testImplementation(libs.junit.api)
  testImplementation(libs.mockk)
  testRuntimeOnly(libs.junit.engine)
}

gradlePlugin.plugins.named(gradlePluginName) {
  description = "Adds markdown task"
}
