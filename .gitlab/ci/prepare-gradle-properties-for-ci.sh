#!/bin/sh

GRADLE_USER_HOME=$(pwd)"/.gradle"
GRADLE_PROPERTIES_FILE="$GRADLE_USER_HOME/gradle.properties"

mkdir -p "$GRADLE_USER_HOME"
export GRADLE_USER_HOME

printf "org.gradle.daemon=false\norg.gradle.caching=true\n" >> "$GRADLE_PROPERTIES_FILE"

if [ -n "$GRADLE_PUBLISH_KEY" ] && [ -n "$GRADLE_PUBLISH_SECRET" ]
then
  printf "gradle.publish.key=%s\ngradle.publish.secret=%s\n" "$GRADLE_PUBLISH_KEY" "$GRADLE_PUBLISH_SECRET" >> "$GRADLE_PROPERTIES_FILE"
else
  printf "The access tokens for the Gradle plugin repository are not defined."
fi


