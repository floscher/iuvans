include(":plugin")
include(":testhelper")

rootDir.resolve("plugin").listFiles { file: File ->
  file.isDirectory && file.resolve("build.gradle.kts").isFile
}?.forEach {
  include(":plugin:${it.name}")
}

rootProject.name = "iuvans"
