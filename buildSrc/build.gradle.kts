import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version libs.versions.kotlin
}

repositories {
  mavenCentral()
  gradlePluginPortal()
}
dependencies {
  implementation(libs.plugin.publish)
  implementation(libs.kotlin.gradlePlugin)
  implementation(libs.jgit)
  implementation(gradleApi())
  implementation(gradleKotlinDsl())

  testImplementation(gradleTestKit())
  testImplementation(libs.mockk)
  testImplementation(libs.junit.api)
  testRuntimeOnly(libs.junit.engine)
}

tasks.withType<KotlinCompile> {
  compilerOptions {
    jvmTarget.set(JvmTarget.JVM_1_8)
  }
}
java.sourceCompatibility = JavaVersion.VERSION_1_8

sourceSets {
  main {
    java {
      srcDir(projectDir.resolve("../plugin/git-version/src/main/kotlin"))
    }
  }
}

tasks.withType<Test> {
  useJUnitPlatform()
}
