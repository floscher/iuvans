import io.mockk.every
import io.mockk.mockk
import org.gradle.api.Project
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class GradleProjectExtensionsTest {

  private fun mockedProject(path: String, name: String): Project {
    val project = mockk<Project>()
    every { project.path }.returns(path)
    every { project.name }.returns(name)
    return project
  }

  @Test
  fun test() {
    val projectPluginRoot: Project = mockedProject(":plugin:iuvans", "iuvans")
    val projectPluginA: Project = mockedProject(":plugin:a", "a")
    val projectPluginTest: Project = mockedProject(":plugin:test", "test")
    val projectPluginTestAbcDefG: Project = mockedProject(":plugin:test:abc-def-g", "abc-def-g")

    assertEquals("dev.spinf.gradle.iuvans", projectPluginRoot.javaPackage)
    assertEquals("dev.spinf.iuvans", projectPluginRoot.gradlePluginId)
    assertEquals("Iuvans", projectPluginRoot.gradlePluginName)

    assertEquals("dev.spinf.gradle.iuvans.a", projectPluginA.javaPackage)
    assertEquals("dev.spinf.iuvans.a", projectPluginA.gradlePluginId)
    assertEquals("A", projectPluginA.gradlePluginName)

    assertEquals("dev.spinf.gradle.iuvans.test", projectPluginTest.javaPackage)
    assertEquals("dev.spinf.iuvans.test", projectPluginTest.gradlePluginId)
    assertEquals("Test", projectPluginTest.gradlePluginName)

    assertEquals("dev.spinf.gradle.iuvans.test.abc_def_g", projectPluginTestAbcDefG.javaPackage)
    assertEquals("dev.spinf.iuvans.test.abc-def-g", projectPluginTestAbcDefG.gradlePluginId)
    assertEquals("AbcDefG", projectPluginTestAbcDefG.gradlePluginName)
  }
}
