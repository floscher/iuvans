
import org.gradle.api.Action
import org.gradle.api.NamedDomainObjectProvider
import org.gradle.api.Project
import org.gradle.plugin.devel.GradlePluginDevelopmentExtension
import org.gradle.plugin.devel.PluginDeclaration

private const val rootName = "iuvans"

private val Project.pluginSubpath: List<String>
  get() {
    if (!path.startsWith(":plugin:")) {
      throw UnsupportedOperationException("Only available in subprojects of the ':plugin' project. (was: ${path})")
    } else if (!path.removePrefix(":plugin").matches("^(:[a-z]([a-z0-9-]*[a-z0-9])?)+$".toRegex())) {
      throw UnsupportedOperationException("""
        Project names must only contain lowercase letters (a-z) , digits (0-9) and hyphens (-).
        They must start with a letter, hyphens can be neither the first nor last character of a project name.
        (was: '$path')""".trimIndent()
      )
    }
    return path.removePrefix(":plugin:").split(':').let {
      if (it.firstOrNull() == rootName) it else listOf(rootName).plus(it)
    }
  }

/**
 * * `/plugin/iuvans` → `dev.spinf.gradle.iuvans`
 * * `/plugin/a` → `dev.spinf.gradle.iuvans.a`
 * * `/plugin/a/b/c-d` → `dev.spinf.gradle.iuvans.a.b.c_d`
 */
val Project.javaPackage: String
  get() = "dev.spinf.gradle.${pluginSubpath.joinToString(".") { it.replace('-', '_') } }"

/**
 * * `/plugin/iuvans` → `dev.spinf.iuvans`
 * * `/plugin/a` → `dev.spinf.iuvans.a`
 * * `/plugin/a/b/c-d` → `dev.spinf.iuvans.a.b.c-d`
 */
val Project.gradlePluginId: String
  get() = "dev.spinf.${pluginSubpath.joinToString(".")}"

/**
 * * `/plugin/iuvans` → `Iuvans`
 * * `/plugin/a` → `A`
 * * `/plugin/a/b/c-d` → `CD`
 */
val Project.gradlePluginName: String
  get() = name.split('-').joinToString("") { it.replaceFirstChar { c -> c.uppercaseChar() } }

val gitVersion = "main"

@Suppress("UnstableApiUsage")
fun Project.registerGradlePlugin(): NamedDomainObjectProvider<PluginDeclaration> {
  val result = extensions.getByType(GradlePluginDevelopmentExtension::class.java).apply {
    website.set("https://gitlab.com/floscher/iuvans/-/blob/${gitVersion}/plugin/git-version/README.md")
    vcsUrl.set("https://gitlab.com/floscher/iuvans.git")
  }.plugins.register(gradlePluginName) {
    it.id = gradlePluginId
    it.implementationClass = "${javaPackage}.${gradlePluginName}Plugin"
    it.displayName = gradlePluginName
    it.tags.addAll("utility", "helper")

    logger.info("Registered Gradle plugin '$gradlePluginId' with implementation class '${it.implementationClass}'")
  }
  tasks.register("pluginInfo") {
    it.actions = listOf(Action {
      logger.lifecycle("""
                     Name:  $gradlePluginName
         Gradle plugin ID:  $gradlePluginId
        Project directory:  $projectDir
             Plugin class:  $javaPackage.${gradlePluginName}Plugin
        Maven coordinates:  $group:$name:$version
      """.trimIndent())
    })
  }
  return result
}
