plugins {
  kotlin("jvm")
}
repositories { mavenCentral() }
dependencies {
  implementation(libs.kotlin.stdlib)
  implementation(gradleTestKit())
  implementation(libs.junit.api)
  implementation(libs.jgit)
}
