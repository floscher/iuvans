package dev.spinf.gradle.iuvans.testhelper

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.PersonIdent
import java.io.File
import java.time.Instant
import java.time.ZoneOffset

val GIT_USER_JOHN_DOE = PersonIdent("John Doe", "john.doe@example.org", Instant.EPOCH, ZoneOffset.UTC)

fun File.initGitInDir(): Git = Git.init().setDirectory(this).call()
fun Git.commit(message: String): Git = apply {
  commit()
    .setAllowEmpty(true)
    .setAuthor(GIT_USER_JOHN_DOE)
    .setCommitter(GIT_USER_JOHN_DOE)
    .setMessage(message)
    .call()
}
fun Git.stageFile(path: String, content: String? = null): Git = apply {
  if (content != null) {
    repository.workTree.resolve(path).writeText(content)
  }
  add().addFilepattern(path).call()
}
fun Git.tag(name: String, message: String? = null) = apply {
  tag()
    .setName(name)
    .setAnnotated(message != null)
    .setMessage(message)
    .setTagger(message?.let { GIT_USER_JOHN_DOE })
    .call()
}
