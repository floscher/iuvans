package dev.spinf.gradle.iuvans.testhelper

import org.gradle.testkit.runner.GradleRunner
import org.junit.jupiter.api.TestInfo
import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.jvm.optionals.getOrNull

object GradleRunnerUtil {
  private val baseTmpDir: Path by lazy {
    Files.createTempDirectory("iuvans-tests_${DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm").format(LocalDateTime.now())}_")
  }

  fun createGradleRunnerInTmpDir(
    testInfo: TestInfo,
    subdir: String? = null,
  ): GradleRunner {
    val path = listOfNotNull(
      testInfo.testClass.getOrNull()?.canonicalName,
      testInfo.testMethod.getOrNull()?.name,
      subdir
    ).fold(baseTmpDir) { acc, s ->
      acc.resolve(s.asFilename).also { it.toFile().mkdirs() }
    }
    println("Created GradleRunner in temporary directory $path")
    return GradleRunner.create().withProjectDir(path.toFile()).withPluginClasspath()
  }
}
