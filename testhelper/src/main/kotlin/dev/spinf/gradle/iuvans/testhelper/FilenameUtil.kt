package dev.spinf.gradle.iuvans.testhelper

private val FILENAME_CHARS = ('a'..'z') + ('A'..'Z') + ('0'..'9') + '_' + '-' + '.'

val String.asFilename: String
  get() = map { if (it in FILENAME_CHARS) it else '_' }
    .joinToString("")
    .replace(Regex("__+"), "_")
    .trim('_')
