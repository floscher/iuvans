package dev.spinf.gradle.iuvans.testhelper

import java.io.File

fun File.writeTextFile(relativePath: String, content: String): File = resolve(relativePath).also {
  println("Create text file $relativePath (${content.toByteArray().size} bytes)")
  it.parentFile.mkdirs()
  it.writeText(content)
}

fun File.appendToTextFile(relativePath: String, content: String): File = resolve(relativePath).also {
  require(it.canWrite())
  println("Appending ${content.toByteArray().size} bytes to text file $relativePath")
  it.appendText(content)
}
