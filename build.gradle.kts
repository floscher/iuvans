import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import dev.spinf.gradle.iuvans.git_version.GitVersionExtension
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  alias(libs.plugins.dependencyUpdates)
  alias(libs.plugins.dokka).apply(false)
}

val gitVersionExtension = extensions.create("gitVersion", GitVersionExtension::class.java)

allprojects {
  group = "dev.spinf.gradle.iuvans"
  version = gitVersionExtension.versionName.get()

  repositories.mavenCentral()

  apply(plugin = "org.jetbrains.dokka")

  tasks.withType(Test::class) {
    useJUnitPlatform()
  }

  plugins.all {
    if (this is JavaBasePlugin) {
      extensions.getByType(JavaPluginExtension::class.java).sourceCompatibility = JavaVersion.VERSION_1_8
    }
  }
  tasks.withType(KotlinCompile::class) {
    compilerOptions {
      this.jvmTarget.set(JvmTarget.JVM_1_8)
    }
  }
}

fun isNonStable(version: String): Boolean = (
  setOf("release", "final", "ga").any { version.contains(it, ignoreCase = true) } ||
  Regex("^[0-9,.v-]+(-r)?$").matches(version)
).not()

tasks.withType(DependencyUpdatesTask::class) {
  rejectVersionIf {
    isNonStable(candidate.version)
  }
}
